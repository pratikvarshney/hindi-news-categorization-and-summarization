#! /usr/bin/env python

import tensorflow as tf
import numpy as np
import os
import time
import datetime
import data_helpers
from text_cnn import TextCNN
from tensorflow.contrib import learn
import csv
from sklearn import metrics

# Parameters
# ==================================================

data_dir = 'data/test/'
class_list = ['automobile', 'business', 'editorial', 'education', 'jokes', 'lifestyle', 'movies', 'sports',
               'technology', 'world']
data_file = [x + '.txt' for x in class_list]


# Data Parameters
# tf.flags.DEFINE_string("positive_data_file", "./ron/pos.txt", "Data source for the positive data.")
# tf.flags.DEFINE_string("negative_data_file", "./ron/neg.txt", "Data source for the positive data.")

# Eval Parameters
tf.flags.DEFINE_integer("batch_size", 64, "Batch Size (default: 64)")
tf.flags.DEFINE_string("checkpoint_dir", "", "Checkpoint directory from training run")
tf.flags.DEFINE_boolean("eval_train", False, "Evaluate on all training data")

# Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")


FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
# print("\nParameters:")
# for attr, value in sorted(FLAGS.__flags.items()):
#     print("{}={}".format(attr.upper(), value))
# print("")

# CHANGE THIS: Load data. Load your own data here
if FLAGS.eval_train:
    x_raw, y_test = data_helpers.load_data_and_labels(data_dir, data_file)
    y_test = np.argmax(y_test, axis=1)
else:
    x_raw = ["महज 11.99 लाख में लांच हुई टाटा हेक्सा, इनोवा क्रिस्‍टा व एक्सयूवी 500 को मिली चुनौती", "अब खराब सामान बेचा तो दु‌कानदारों की खैर नही, होगी दो साल की कैद"]
    y_test = [0, 1]

print("\n{} - Processing started\n".format(datetime.datetime.now()))
print("Number of examples: {}".format(len(x_raw)))

# Map data into vocabulary
vocab_path = os.path.join(FLAGS.checkpoint_dir, "..", "vocab")
vocab_processor = learn.preprocessing.VocabularyProcessor.restore(vocab_path)
x_test = np.array(list(vocab_processor.transform(x_raw)))

# Evaluation
# ==================================================
checkpoint_file = tf.train.latest_checkpoint(FLAGS.checkpoint_dir)
# checkpoint_file = 'runs/1493436792/checkpoints/model-6400'
graph = tf.Graph()
with graph.as_default():
    session_conf = tf.ConfigProto(
      allow_soft_placement=FLAGS.allow_soft_placement,
      log_device_placement=FLAGS.log_device_placement)
    sess = tf.Session(config=session_conf)
    with sess.as_default():
        # Load the saved meta graph and restore variables
        saver = tf.train.import_meta_graph("{}.meta".format(checkpoint_file))
        saver.restore(sess, checkpoint_file)

        # Get the placeholders from the graph by name
        input_x = graph.get_operation_by_name("input_x").outputs[0]
        # input_y = graph.get_operation_by_name("input_y").outputs[0]
        dropout_keep_prob = graph.get_operation_by_name("dropout_keep_prob").outputs[0]

        # Tensors we want to evaluate
        predictions = graph.get_operation_by_name("output/predictions").outputs[0]

        # Generate batches for one epoch
        batches = data_helpers.batch_iter(list(x_test), FLAGS.batch_size, 1, shuffle=False)

        # Collect the predictions here
        all_predictions = []

        for x_test_batch in batches:
            batch_predictions = sess.run(predictions, {input_x: x_test_batch, dropout_keep_prob: 1.0})
            all_predictions = np.concatenate([all_predictions, batch_predictions])

# Print accuracy if y_test is defined
confusion = [[0 for x in range(len(class_list))] for y in range(len(class_list))]
if y_test is not None:
    for i in range(len(y_test)):
        confusion[int(y_test[i])][int(all_predictions[i])] += 1
    correct_predictions = float(sum(all_predictions == y_test))
    print("\n# Accuracy: {:g}".format(correct_predictions/float(len(y_test))))
    # calc precision, recall and f1-avg (macro)
    tp = [0 for x in range(len(class_list))]
    tn = [0 for x in range(len(class_list))]
    fp = [0 for x in range(len(class_list))]
    fn = [0 for x in range(len(class_list))]

    pre = [0 for x in range(len(class_list))]
    rec = [0 for x in range(len(class_list))]
    f1 = [0 for x in range(len(class_list))]

    f1_avg = 0

    row_sum = [0 for x in range(len(class_list))]
    col_sum = [0 for x in range(len(class_list))]

    print('\nConfusion:')
    for i in range(len(class_list)):
        print('{:<10}'.format(class_list[i]), confusion[i])
        for j in range(len(class_list)):
            row_sum[i] += confusion[i][j]
            col_sum[j] += confusion[i][j]

    print()

    print('{:<10} {:<24} {:<24} {:<24}'.format('Class', 'Precision', 'Recall', 'F1'))
    for i in range(len(class_list)):
        tp[i] = confusion[i][i]
        fp[i] = col_sum[i] - tp[i]
        fn[i] = row_sum[i] - tp[i]
        # tn[i] = total_lines - tp[i]
        if tp[i] == 0:
            pre[i] = 0
            rec[i] = 0
            f1[i] = 0
        else:
            pre[i] = tp[i] / (tp[i] + fp[i])
            rec[i] = tp[i] / (tp[i] + fn[i])
            f1[i] = (2 * pre[i] * rec[i]) / (pre[i] + rec[i])
        print('{:<10} {:<24} {:<24} {:<24}'.format(class_list[i], pre[i], rec[i], f1[i]))

    f1_avg = sum(f1) / len(class_list)
    print("F1 avg. (macro):", f1_avg)

    print()
    print('sklearn: confusion_matrix')
    print(metrics.confusion_matrix(y_test, all_predictions))
    print()
    print('sklearn: classification_report')
    print(metrics.classification_report(y_test, all_predictions, target_names=class_list))

if not FLAGS.eval_train:
    print()
    for i in range(len(x_raw)):
        print('Prediction: {:<20} Original: {:<20} Input: {}'.format(data_file[int(all_predictions[i])][:-4], data_file[y_test[i]][:-4] ,x_raw[i]))

# Save the evaluation to a csv
predictions_human_readable = np.column_stack((np.array(x_raw), all_predictions))
out_path = os.path.join(FLAGS.checkpoint_dir, "..", "prediction.csv")
print("\nWriting {0}".format(out_path))
with open(out_path, 'w') as f:
    csv.writer(f).writerows(predictions_human_readable)
