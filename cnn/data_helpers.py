import numpy as np
import re
import itertools
from collections import Counter

def load_data_and_labels(data_dir, data_file):
    """
    Loads MR polarity data from files, splits the data into words and generates labels.
    Returns split sentences and labels.
    """
    # Load data from files
    examples = []
    for file in data_file:
        l = list(open(data_dir + file, mode='r', encoding='utf-8').readlines())
        examples.append([s.strip() for s in l if s])

    # Split by words
    x_text = []
    for l in examples:
        x_text += l
        
    # x_text = [clean_str(sent) for sent in x_text]
    
    # Generate labels
    labels = []
    for i in range(len(examples)):
        label = [0] * len(examples)
        label[i] = 1
        labels.append([label for _ in examples[i]])
    y = np.concatenate(labels, 0)
    return [x_text, y]


def batch_iter(data, batch_size, num_epochs, shuffle=True):
    """
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int((len(data)-1)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]
