import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from collections import Counter
import pickle

def preproc1(rawdata):
    #Data preprocessed and cleaned
    rawdata=str(rawdata).lower()
    rawdata = rawdata.replace("-"," ")
    rawdata = rawdata.replace("."," ")
    rawdata = rawdata.replace(","," ")
    rawdata = rawdata.replace("'"," ")
    rawdata = rawdata.translate(None, '?><|{}[]!@#$%^&*_+=`~".():;-/\'1234567890,abcdefghijklmnopqrstuvwxyz\n')
    rawdata = rawdata.replace(" st ","")
    rawdata = rawdata.replace(" nd ","")
    rawdata = rawdata.replace(" rd ","")
    rawdata = rawdata.replace(" th ","")
    rawdata = rawdata.replace( u"\u0964" ," ")
    rawdata = rawdata.replace("  "," ")
    rawdata = rawdata.replace("  "," ")
    rawdata = rawdata.replace("  "," ")
    return rawdata

with open("tokensnbt.txt","r") as f2:
    heads,desc=pickle.load(f2)
bigstring=""
for string in heads:
    bigstring+=preproc1(string.strip())
for string in desc:
    bigstring+=preproc1(string.strip())
bigstring=bigstring.split(" ")
#Dictionary of word and their counts is created.
cc=Counter(bigstring)
print cc

cc=dict(cc)
bigstring=""
for key in cc.keys():
    bigstring+=str(key)+" "+str(cc[key])+"\n"
    print key,cc[key]
f=open('bagofwordsnbt.txt','w')
f.write(bigstring)
