import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import glob,json,pickle
files=glob.glob("/home/elmaestro/NLP_Project/data/*")

heads=[]
desc=[]
#cleanig the data
def preproc(rawdata):
    rawdata=str(rawdata).lower()
    rawdata = rawdata.replace("-"," ")
    rawdata = rawdata.replace("."," ")
    rawdata = rawdata.replace(","," ")
    rawdata = rawdata.replace("'"," ")
    rawdata = rawdata.translate(None, '?><|{}[]!@#$%^&*_+=`~".():;-/,abcdefghijklmnopqrstuvwxyz\n')
    rawdata = rawdata.replace(" st ","")
    rawdata = rawdata.replace(" nd ","")
    rawdata = rawdata.replace(" rd ","")
    rawdata = rawdata.replace(" th ","")
    rawdata = rawdata.replace("  "," ")
    rawdata = rawdata.replace("  "," ")
    rawdata = rawdata.replace("  "," ")
    return rawdata
print files
#load data from multiple files and extract from json
for fname in files:
    f = open(fname,"r")
    a=json.load(f)
    f.close()
    heads.append(a["news"]["Hindi-Headline"])
    desc.append(a["news"]["news_body"])
print len(heads)
#store file in pickle
FN0 = 'tokens' # this is the name of the data file which I assume you already have
with open("tokens.txt","w+") as f:
    pickle.dump((heads,desc),f)
f.flush()

    
