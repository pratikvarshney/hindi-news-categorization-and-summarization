
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from tqdm import tqdm
import numpy as np
import scipy.spatial.distance as distance
import json,pickle
from sklearn.decomposition import TruncatedSVD

def get_embedding_index():
    embeddings_index = {}
    f = open('mymodel.txt')
    for line in tqdm(f):
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()

    print('Found %s word vectors.' % len(embeddings_index))
    return embeddings_index

embeddings_index = get_embedding_index()

#reference to: https://github.com/YingyuLiang/SIF/blob/master/src/data_io.py
def getWordWeight(weightfile, a=1e-3):
    if a <=0: # when the parameter makes no sense, use unweighted
        a = 1.0

    word2weight = {}
    with open(weightfile) as f:
        lines = f.readlines()
    N = 0
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split()
            if(len(i) == 2):
                word2weight[i[0]] = float(i[1])
                N += float(i[1])

    for key, value in word2weight.iteritems():
        word2weight[key] = a / (a + value/N)

    for word in embeddings_index.keys():
        if word not in word2weight:
            word2weight[word] = 1.0
    return word2weight

word2weight = getWordWeight('bagofwordsnbt.txt')

def clean_word(word):
    w = word.strip()
    if(len(w)>1):
        return w.replace("?","").replace(".","").replace("!","").replace(",","")
    else:
        return w

def getWeightedSentenceFeatures(embeddings_index, word2weight, sentence):
    """
    Obtain the sentence feature for sentiment analysis by averaging its
    word vectors

    # Inputs:
    # embeddings_index -- a dictionary that maps words to their glove embedding
    #
    # Output:
    # - sentVector: feature vector for the sentence
    """
    dimension = 100
    sentVector = np.zeros(dimension)
    reasonable_count = 0
    sentence_sequence = sentence.split()
    for word in sentence_sequence:
        temp_word = word.lower()
        temp_word = clean_word(temp_word)
        if temp_word in embeddings_index:
            reasonable_count+=1
            sentVector += embeddings_index[temp_word] * word2weight[temp_word]
    #if all the words in sentence are unusual
    if reasonable_count == 0:
        sentVector = np.random.rand(dimension)
    else:
        sentVector = sentVector/reasonable_count

    return sentVector
#reference to: https://github.com/YingyuLiang/SIF/blob/master/src
def compute_pc(X,npc=1):
    """
    Compute the principal components
    :param X: X[i,:] is a data point
    :param npc: number of principal components to remove
    :return: component_[i,:] is the i-th pc
    """
    svd = TruncatedSVD(n_components=npc, n_iter=7, random_state=0)
    svd.fit(X)
    return svd.components_

def build_component(list_sentence):
    """
    Input:
    list_sentence: e.g. ["asdf asdf asdf.","asdf asdf"]

    Output:
    temporary matrix for later calculation, np.dot(pc.transpose(),pc)
    100 * 100 matrix
    """
    X = np.zeros((len(list_sentence), 100))
    for index, candidate in enumerate(list_sentence):
        X[index,:] = getWeightedSentenceFeatures(embeddings_index, word2weight, candidate)
    pc = compute_pc(X)
    return np.dot(pc.transpose(),pc)

def preproc1(rawdata):
    #data preprocessed and cleaned.
    rawdata=str(rawdata).lower()
    rawdata = rawdata.replace("-"," ")
    rawdata = rawdata.replace("."," ")
    rawdata = rawdata.replace(","," ")
    rawdata = rawdata.replace("'"," ")
    rawdata = rawdata.translate(None, '?><|{}[]!@#$%^&*_+=`~".():;-/,abcdefghijklmnopqrstuvwxyz\n')
    rawdata = rawdata.replace(" st ","")
    rawdata = rawdata.replace(" nd ","")
    rawdata = rawdata.replace(" rd ","")
    rawdata = rawdata.replace(" th ","")
    rawdata = rawdata.replace( u"\u0964" ," ")
    rawdata = rawdata.replace("  "," ")
    rawdata = rawdata.replace("  "," ")
    rawdata = rawdata.replace("  "," ")
    return rawdata


with open("tokensnbt.txt","r") as f2:
    heads,desc=pickle.load(f2)
list_sentence=[]
print desc[0]
for para in desc:
    [list_sentence.append(preproc1(x)) for x in para.split(u"\u0964")]
print len(heads)
temp_component = build_component(list_sentence)

def getSentenceFeatures(embeddings_index, word2weight, sentence):
    #get weight sentence vector
    vector = getWeightedSentenceFeatures(embeddings_index, word2weight, sentence)
    final_vector = vector - np.dot(vector.reshape((1,100)), temp_component).reshape((100,))
    return final_vector

def caculate_euclidean_similarity(s1, s2):
    '''
    input s1, s2: str, sentences
    output score: float, final score for euclidean distance of s1 and s2
    '''
    s1_vec = getSentenceFeatures(embeddings_index, word2weight, s1)
    s2_vec = getSentenceFeatures(embeddings_index, word2weight, s2)
    try:
        # score = distance.cosine(s1_vec,s2_vec)
        score = -1*distance.euclidean(s1_vec,s2_vec)
    except:
        print s1
        print s2
        print s1_vec
        print s2_vec
        print "something wrong with sif_method"
        score = 0
    return score


def caculate_cosine_similarity(s1, s2):
    '''
    input s1, s2: str, sentences
    output score: float, final score for cosine of s1 and s2
    '''
    s1_vec = getSentenceFeatures(embeddings_index, word2weight, s1)
    s2_vec = getSentenceFeatures(embeddings_index, word2weight, s2)
    try:
        score = 1 - distance.cosine(s1_vec,s2_vec)
    except:
        print s1
        print s2
        print s1_vec
        print s2_vec
        print "something wrong with sif_method"
        score = 0
    return score

def get_dup_score(query_list, candidate_list):
    result = []
    candidate_len = len(candidate_list)
    for query in query_list:
        y = [caculate_cosine_similarity(query, candidate) for candidate in candidate_list]
        result.append(y)
    return result


def findMostProbSentence(title,para):

    para = [preproc1(x) for x in para.split(u"\u0964")]
    max=-9999999
    sentence=""
    for x in para:
        val=caculate_cosine_similarity(title,x)
        if val>max:
            max=val
            sentence=x

    return sentence
# find most probable summary
def generate_summaries():
    writetofile=""
    for i in range(3):
        writetofile+=heads[i]+" -> "+findMostProbSentence(heads[i],desc[i])+"\n"
    print writetofile
generate_summaries()


