import re
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import nltk
import json
import re
import tf_glove
model = tf_glove.GloVeModel(embedding_size=100, context_size=10, min_occurrences=25,
                            learning_rate=0.05, batch_size=512)

def extract_reddit_comments(path):
    # A regex for extracting the comment body from one line of JSON (faster than parsing)
    body_snatcher = re.compile(r"\{.*?(?<!\\)\"body(?<!\\)\":(?<!\\)\"(.*?)(?<!\\)\".*}")
    with open(path) as file_:
        for line in file_:
            match = body_snatcher.match(line)
            if match:
                body = match.group(1)
                # Ignore deleted comments
                if not body == '[deleted]':
                    # Return the comment as a string (not yet tokenized)
                    yield body

def tokenize_comment(comment_str):
    # Use the excellent NLTK to tokenize the comment body
    #
    # Note that we're lower-casing the comments here. tf_glove is case-sensitive,
    # so if you want 'You' and 'you' to be considered the same word, be sure to lower-case everything.
    return nltk.wordpunct_tokenize(comment_str.lower())

def reddit_comment_corpus(path):
    # A generator that returns lists of tokens representing individual words in the comment
    return (tokenize_comment(comment) for comment in extract_reddit_comments(path))

# Replace the path with the path to your corpus file
#corpus = reddit_comment_corpus("/media/grady/PrimeMover/Datasets/RC_2015-01-1m_sample")
stro=u"\u0964"
def myfun(fname):
    #f = open(fname,"r")
    a=json.loads(fname)
    #f.close()
    a=a['body']
    a = a.replace(stro,"")
    a=re.sub("[\?\>\<\|\{\}\[\]\!\@\#\$\%\^\&\*\_\+\=\`\~\"\.\(\)\:\;\-0123456789/]", "", a, 0, 0)
    a = a.replace(','," ,")
    a=a.replace("\n","")
    a=a.split(" ")
    return a

#for i in a: print i

#import glob
#files=glob.glob("/home/venky/IdeaProjects/TextSumm/src/json/*.*")
corpus=[]
number_of_files=10000
f=open('bigdata.txt','r')
for i in f:
    #print i
    corpus.append(myfun(i))
print len(corpus)

model.fit_to_corpus(corpus)
model.train(num_epochs=50, log_dir="log/example", summary_batch_interval=1000)
print model.embedding_for(u"\u0939\u0948")
#print (model.embedding_for(u"\u0939\u0948")

embeddings=model.embeddings
words=model.words

myglove=""

for word in words:
    myglove+=word
    for val in embeddings[model.id_for_word(word)]:
        myglove+=" "+str(val)
    myglove+="\n"
import pickle
with open('mymodel.txt','w') as f:
    f.write(myglove)